from flask import Flask
from flask import jsonify
import os
from datetime import datetime
from random import randint

app = Flask(__name__)


@app.route("/")
def index():
    return jsonify(hello="k8", datetime=datetime.now(), random=randint(100, 200))


if __name__ == "__main__":  # pragma: no cover
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host="0.0.0.0", port=port)
