FROM alpine:latest
RUN apk add --no-cache python3
COPY . /code
WORKDIR code
RUN pip3 install -r requirements.txt
RUN adduser --disabled-password app_user
USER app_user
ENTRYPOINT ["python3"]
CMD ["app/hello.py"]
