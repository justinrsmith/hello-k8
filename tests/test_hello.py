import json
import pytest

from app import hello


def test_index():
    flask_app = hello.app.test_client()

    response = flask_app.get("/")
    resp_json = response.get_json()
    assert "hello" in resp_json.keys()
    assert "datetime" in resp_json.keys()
