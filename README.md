# hello-k8

A repository that leverages Gitlab's CI/CD functionality to deploy a dockerized
Flask microservice to Kubernetes.

### Features
- Leverages `docker-compose` to build a multi-container Docker application
    - Services
        - `web`: the Flask microservice that is deployed to Kubernetes
        - `tests`: unit tests for the Flask microservice
        - `black`: runs `black` formatter on the source code
- When commiting changes to `master` Gitlab will kickoff a pipelne
    - Pipeline steps
        - `build`: build and push the `web` service to GCR
        - `test`: run unit tests and stop pipeline if there is a failure
        - `deploy`: deploy new docker image to GKE
